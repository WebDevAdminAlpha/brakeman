#!/bin/sh

USER="$TRACKING_CALCULATOR_USER"
TOKEN="$TRACKING_CALCULATOR_TOKEN"

PROJECT_ID="24257614"
VERSION="2.0.0"

FILENAME="analyzer-v${VERSION}.tar.gz"

curl --user "$USER:$TOKEN" \
     "https://gitlab.com/api/v4/projects/$PROJECT_ID/packages/generic/analyzer/$VERSION/$FILENAME" \
     --output "$FILENAME" --silent --show-error --fail || exit 1

tar xvf "$FILENAME" -C /tmp && mv /tmp/analyzer /analyzer-tracking && exit 0

exit 1

