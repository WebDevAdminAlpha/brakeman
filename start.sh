#!/bin/sh

REPORT="$CI_PROJECT_DIR/gl-sast-report.json"
OUTPUT="$CI_PROJECT_DIR/gl-sast-report-post.json"
FEATURE_FLAG="vulnerability_finding_signatures"

/analyzer-brakeman "$@"

echo "$GITLAB_FEATURES" | grep -q "$FEATURE_FLAG" || exit 0

[ -f "$REPORT" ] && {
    echo "running post analyzer"
    /analyzer-tracking process \
    --report "$REPORT" \
    --output "$OUTPUT" \
    --repository "$CI_PROJECT_DIR" \
    && mv "$OUTPUT" "$REPORT"
}

exit 0
