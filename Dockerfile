FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

ARG TRACKING_CALCULATOR_TOKEN
ENV TRACKING_CALCULATOR_TOKEN=$TRACKING_CALCULATOR_TOKEN

ARG TRACKING_CALCULATOR_USER
ENV TRACKING_CALCULATOR_USER=$TRACKING_CALCULATOR_USER

RUN apk update && apk add curl

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
  PATH_TO_MODULE=`go list -m` && \
  go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer-brakeman

RUN [ -n "$TRACKING_CALCULATOR_TOKEN" ] && /go/src/buildapp/config/fetch_tracking_calculator.sh

FROM ruby:2.7-alpine

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-5.0.0}

RUN apk update
RUN apk add --no-cache git
RUN apk add musl
RUN apk upgrade

RUN gem update webrick
RUN gem install brakeman -v $SCANNER_VERSION

COPY --from=build /analyzer-brakeman /analyzer-brakeman
COPY --from=build /analyzer-tracking /analyzer-tracking

ENTRYPOINT []
ADD start.sh /analyzer
RUN chmod +x /analyzer
CMD ["/analyzer", "run"]
